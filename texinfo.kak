declare-option -hidden str texinfo_source %sh{dirname "$kak_source"}
declare-option -hidden range-specs texinfo_link_data
declare-option -hidden range-specs texinfo_link_highlights

add-highlighter shared/texinfo group
add-highlighter shared/texinfo/ regex '^File: [^\n]*' 0:meta
add-highlighter shared/texinfo/ regex '\n[^\n]+\n[-=*]+\n' 0:header
add-highlighter shared/texinfo/ ranges texinfo_link_highlights

hook global WinSetOption filetype=texinfo %{
    add-highlighter window/texinfo ref texinfo

    hook -once -always window WinSetOption filetype=.* %{
        remove-highlighter window/texinfo
    }
}

define-command texinfo-collect-link-data -params 1 -hidden \
    -docstring "Collect results found by texinfo-scrape-links into options." \
%{
    # We have the details for all the matches we've found
    # in the s, a, b, c, d, e, and f registers.
    # We need to recombine them
    # into the right format for the texinfo_link_highlights
    # and texinfo_link_data options.
    # There's two difficulties here:
    #
    # - Because these registers could potentially be very large,
    #   too large to pass as arguments or environment variables,
    #   it's difficult to process them outside Kakoune
    # - Because Kakoune represents a buffer as a vector of lines,
    #   pasting these registers into a single line
    #   is extremely slow,
    #   making it difficult to process them inside Kakoune.
    #
    # So we resort to a somewhat complicated dance
    # to get just the selection coordinates into a file,
    # but with newline-separation,
    # so we can load it into a buffer
    # and add all the other registers to each line.

    # Write the selections in register s
    # all into a temporary file
    echo -to-file "%arg{1}/raw-selections" %reg{s}

    # Put each selection on its own line.
    nop %sh{ tr ' ' '\n' < "$1/raw-selections" > "$1/selection-per-line" }

    evaluate-commands -save-regs r %{
        try %{
            # Read all the selections into a new scratch buffer.
            edit "%arg{1}/selection-per-line"
            rename-buffer -scratch %arg{1}
            # Select each line individually
            # and don't include the trailing newline.
            execute-keys <percent><a-s>H
            # Append the face name to make this a range-specs item.
            execute-keys a|link<esc>
            # Drop the first selection, which is a dummy item
            execute-keys )<a-,>
            # Yank the completed strings in to "r (for "result")
            execute-keys <">ry
            # Switch back to the original buffer
            delete-buffer
            # Add these new range-specs to the link highlights
            set-option -add buffer texinfo_link_highlights %reg{r}
        } catch %{
            echo -debug texinfo collect link highlights Ignored error: %val{error}
            delete-buffer
        }

        try %{
            edit "%arg{1}/selection-per-line"
            rename-buffer -scratch %arg{1}
            execute-keys <percent><a-s>H
            # Insert the pipe delimiter after each selection
            execute-keys a|
            # Bare infonames are in "a,
            # infonames from quoted external refs are in "b,
            # and infonames from unquoted external refs are in "c,
            # so it's impossible for more than one to be non-empty at once.
            execute-keys <c-r>a<c-r>b<c-r>c
            # A space to separate the infoname from the target
            execute-keys <space>
            # External ref quoted target names are in "d,
            # external ref unquoted target names are in "e,
            # local quoted target names are in "f,
            # local unquoted target names are in "g
            # it's impossible for more than one to be non-empty at once.
            execute-keys <c-r>d<c-r>e<c-r>f<c-r>g
            # Right now,
            # there is a newline between each item,
            # but some items might *end* with a newline that we want to delete.
            # To delete the newlines,
            # we'd have to save the selections (Z),
            # make the change,
            # then restore the selections (z).
            # But because of how Kakoune calculates diffs
            # to restore selections after a modification,
            # there's a good chance it'll think we deleted the separator
            # and keep the newline as part of the selection.
            # To demonstrate to the diff algorithm
            # that we don't want newlines in our selections,
            # we'll add a fence-post character that's *not* selected,
            # and then when we restore selections
            # Kakoune will see it hasn't moved and keep it not-selected.
            execute-keys X<esc>H
            # Let's remove all newlines,
            # in case a link wrapped across two lines.
            try %{ execute-keys Zs\n<ret>r<space>z }
            # If a link wrapped across two lines
            # in an indented paragraph,
            # now we'll have a bunch of extra spaces we need to remove.
            # If we remove them with s<space>+<ret>c<space><esc>
            # then there's a moment where the selection should be 0-width
            # which means Kakoune will change it unpredictably
            # to keep it minimum-1-width.
            # Instead,
            # we'll delete all but one of every span of two-or-more spaces.
            try %{ execute-keys Zs<space><space>+<ret>Hdz }
            execute-keys )<a-,>
            execute-keys <">ry
            delete-buffer
            set-option -add buffer texinfo_link_data %reg{r}
        } catch %{
            echo -debug texinfo collect link data Ignored error: %val{error}
            delete-buffer
        }
    }
}

define-command texinfo-scrape-link-pattern -hidden -params 2 \
    -docstring "
        texinfo-scrape-link-pattern CONTEXT PATTERN

        Search the current buffer for PATTERN, storing match details into
        registers. Any errors are logged with CONTEXT and ignored.
        "\
%{
    try %{
        # Set the default search register,
        # so we do not need to key-escape an arbitrary regex
        set-register slash %arg{2}
        # Search for the default regex
        execute-keys <percent>s<ret>
        # Make the first match the active selection,
        # so selections_desc will be in document order.
        execute-keys )
        # Add each selection to the list of selections
        set-register s %reg{s} %val{selections_desc}
        # Add all the infonames groups to the collection
        set-register a %reg{a} %reg{2}
        set-register b %reg{b} %reg{3}
        set-register c %reg{c} %reg{5}
        # Add all the quoted node name groups
        set-register d %reg{d} %reg{4}
        set-register e %reg{e} %reg{7}
        # Add all the bare node name groups
        set-register f %reg{f} %reg{6}
        set-register g %reg{g} %reg{8}
    } catch %{
        echo -debug "%arg{1} ignored error: %val{error}"
    }
}

define-command texinfo-scrape-links -hidden \
    -docstring "Collect all node links into the texinfo_link_data option." \
%{
    evaluate-commands %sh{
        kakquote() { printf "%s\n" "$*"|sed -e "s/'/''/g;1s/^/'/;\$s/\$/' /"; }

        # Lexical tokens used in TeXinfo link syntax.
        QUOTED='\x7F([^\x7F]*?)\x7F'
        INFONAME='\(([^\x28\x29]+?)\)'
        ALIAS='[^\x7F:]+?'
        NODENAME='([^\x7F\x28.,:\n][^.,:]*?)'
        WS='\s+'
        HEADER_END='[,.\n]'
        ALIAS_END='[.,]'
        VERBATIM_END='::'
        INLINE='(?:^\*|\*[Nn]ote)'
        HEADER='(?:Next:|Prev:|Up:)'

        EMPTY="()"
        QUOTED_OR_ALIAS="(?:$QUOTED|$ALIAS)"

        # A reference can be in one of three forms:
        # - A lone infoname links to the "Top" target: (dir)
        # - an infoname and a target: (dir)Top
        # - a lone target links to the current info document: Top
        REF="(?:$INFONAME|$INFONAME$QUOTED|$INFONAME$NODENAME|$QUOTED|$NODENAME)"

        # There are three contexts where a link can appear:
        # - in the headers of each page
        # - an inline link displayed as is
        # - an inline link with a human-readable alias
        #
        # So we can pull out the correct group from each match,
        # it's incredibly important
        # that each of these regexes
        # has exactly the same number of groups before ${1}
        # so we add $EMPTY where needed.
        header_re() {
            printf '%s' "$HEADER$WS$EMPTY\K$1(?=$HEADER_END)"
        }
        verbatim_re() {
            printf '%s' "$INLINE$WS$EMPTY\K$1(?=$VERBATIM_END)"
        }
        alias_re() {
            printf '%s' "$INLINE$WS$QUOTED_OR_ALIAS:$WS\K$1(?=$ALIAS_END)"
        }

        # A register cannot have zero items, it can only have an empty string.
        # When pasting, an empty-string item is ignored
        # (since Kakoune can't have a zero-width selection)
        # but it is included when inserting text from a register with <c-r>.
        # Since we want to mix pasting and <c-r>,
        # and since we want to extend each register
        # with 'reg s %reg{s} ...',
        # we need to ensure each register is initialised
        # to a dummy value we can later ignore.
        printf 'set-register %s dummy\n' s a b c d e f g

        for context in header_re verbatim_re alias_re; do
            regex=$("$context" "$REF")
            printf 'texinfo-scrape-link-pattern %s %s\n' \
                "$(kakquote "texinfo-scrape-links $context")" \
                "$(kakquote "$regex")"
        done
    }
}

define-command texinfo-load-infofile -hidden -params 2 \
    -docstring "Open the given file as a texinfo buffer and populate it" \
%{
    # Load it into Kakoune
    edit "%arg{1}/infofile"

    # Make room to record the position and target
    # of all links in the document.
    set-option buffer texinfo_link_data %val{timestamp}
    set-option buffer texinfo_link_highlights %val{timestamp}

    evaluate-commands -draft -save-regs /sabcdefg %{
        # Collect all the link data into temporary registers
        texinfo-scrape-links
        # Combine the register values into the texinfo_link_* options.
        texinfo-collect-link-data %arg{1}
    }

    # Clean up the buffer content.
    try %{ exec '%s\x00\x08\[.*?\x00\x08\]\n<ret>d' }
    try %{ exec <percent>s\x7F<ret>d }
    update-option buffer texinfo_link_data
    update-option buffer texinfo_link_highlights

    # Make the buffer presentable
    rename-buffer -scratch %arg{2}
    set buffer readonly true
    set window filetype texinfo
}

define-command texinfo-ensure-buffer-for-infofile -hidden -params 2 \
    -docstring "Ensure a buffer for the named TeXinfo manual exists." \
%{
    # If we already have a prepared buffer, switch to it.
    try %{
        buffer "*texinfo-%arg{1}*"

    # Otherwise, create one.
    } catch %{
        nop %sh{
            kakquote() {
                printf "%s\n" "$*"|sed -e "s/'/''/g;1s/^/'/;\$s/\$/' /"
            }

            # Generate the plain-text version of the manual.
            tempdir=$(mktemp -d -t kak-texinfo.XXXXXX)
            trap 'rm -rf "$tempdir"' EXIT

            "$kak_opt_texinfo_source"/texinfo-helper.sh format "$2" \
                > "$tempdir"/infofile

            # Tell Kakoune to load the processed file
            # so we can remove the tempfile when it's done.
            printf '
                texinfo-load-infofile %s %s
                echo -to-file %s x
                ' \
                "$(kakquote "$tempdir")" \
                "$(kakquote "*texinfo-$1*")" \
                "$kak_response_fifo" \
                > "$kak_command_fifo"
            read _ignored < "$kak_response_fifo"
        }
    }
}

define-command texinfo-jump-to-node -hidden -params 1 \
    -docstring "Jump to the named node in the active TeXinfo manual" \
%{
    evaluate-commands -save-regs "/" %sh{
        kakquote() { printf "%s\n" "$*"|sed -e "s/'/''/g;1s/^/'/;\$s/\$/' /"; }

        printf "reg slash %s\n" \
            "$(kakquote '^File: [^,]*,\h+Node: \Q'"$1")"
        printf "execute-keys '/<ret>ghvt'\n"
    }
}

define-command texinfo -params 0..2 -docstring "
    texinfo [<manual> [<node>]]: Browse TeXinfo documentation

        <manual> is the name of an installed manual, or the
        filesystem path to one. If not supplied, it defaults
        to the top-level index of installed manuals.

        <node> is the name of a node (page) within the given
        manual. If not supplied, it defaults to the top-level
        index of the given manual.
    " \
%{
    evaluate-commands %sh{
        kakquote() { printf "%s\n" "$*"|sed -e "s/'/''/g;1s/^/'/;\$s/\$/' /"; }

        "$kak_opt_texinfo_source"/texinfo-helper.sh locate "$1" | {
            IFS="$(printf '\t')" read -r infofile nodename

            infoname=$(basename "$(basename "$infofile" .gz)" .info)

            # If $1 is the alias of a particular node, use it.
            # Otherwise, if we were give a specific node, use it.
            # Otherwise, default to "Top".
            nodename=${nodename:-${2:-Top}}

            if [ -z "$infofile" ]; then
                printf "%s\n" "fail No such TeXinfo manual $1"
                exit
            fi

            printf "
                evaluate-commands -try-client %s %%{
                    texinfo-ensure-buffer-for-infofile %s %s
                    texinfo-jump-to-node %s
                }
                " \
                "$(kakquote "$kak_opt_docsclient")" \
                "$(kakquote "$infoname")" \
                "$(kakquote "$infofile")" \
                "$(kakquote "$nodename")"
        }
    }
}

complete-command texinfo shell-script-candidates %{
    case $kak_token_to_complete in
        0)
            "$kak_opt_texinfo_source"/texinfo-helper.sh lsaliases | cut -f1
            ;;
        1)
            # Try to extract the manual and node name the first argument
            # expands to.
            "$kak_opt_texinfo_source"/texinfo-helper.sh locate "$1" | {
                # It's important that `read` executes in the same subshell
                # that executes the `if` below, so that it can check
                # the variables that `read` sets.
                IFS="$(printf '\t')" read -r infofile nodename

                # The first argument is just a manual, so let's complete
                # possible node names for this manual.
                if [ -z "$nodename" ]; then
                    "$kak_opt_texinfo_source"/texinfo-helper.sh \
                        lsnodes "$infofile"
                fi
            }
            ;;
    esac
}
