Engine:

  - Use the new selection_count expansion
    to create multiple lines in texinfo-collect-link-data,
    to avoid a temporary file

Mappings:

  - `gu` mapping to follow the "Up:" link in the header
  - `gn` mapping to prompt for a node to go to?
  - `<ret>` to follow a node reference.

Highlighting:

  - Node references: link

Fancy:

  - Rewrite node references to hide the infoname and nodename,
    and show only the alias (if any),
    like how Kakoune renders AsciiDoc files.
