#!/bin/sh
INFOPATH=${INFOPATH:-PATH}
PATH=${PATH:-/usr/local/bin:/usr/bin:/bin}

warn() {
    printf "%s\n" "$*" >&2
}

err() {
    warn "$@"
    exit 1
}

split_path() {
    (
    set -o noglob
    IFS=:
    set -- $1
    printf "%s\n" "$@"
    )
}

list_info_dirs() {
    split_path "$INFOPATH" |
        while read -r infodir; do
            if [ "PATH" = "$infodir" ]; then
                split_path "$PATH" |
                    while read -r bindir; do
                        prefix=$(dirname "$bindir")
                        printf "%s\n" "$prefix/share/info" "$prefix/info"
                    done
            else
                printf "%s\n" "$infodir"
            fi
    done
}

info_files_in_dir() {
    if [ -d "$1" ]; then
        find "$1" \
            -name "*.info.gz" -o \
            -name "*.info" -o \
            -name "dir"
    fi
}

read_info_file() {
    case "$1" in
        *.gz)
            zcat "$1"
            ;;
        *)
            cat "$1"
            ;;
    esac
}

names_for_info_dir() {
    (
        info_files_in_dir "$1"
        echo
        cat "$1/dir"
    ) |
        awk -v "infodir=$infodir" '
            BEGIN { OFS="\t"; state="filenames" }

            # When we hit the blank line
            # between filenames and the dir file contents,
            # switch to the dir-file-reading state.
            state == "filenames" && /^$/ { state = "infodir"; getline }

            state == "filenames" {
                if (match($0, "[^/]*$")) {
                    basename = substr($0, RSTART, RLENGTH)

                    if (match(basename, "\.gz$")) {
                        basename = substr(basename, 1, RSTART-1)
                    }
                    if (match(basename, "\.info$")) {
                        basename = substr(basename, 1, RSTART-1)
                    }

                    # We have found the manual name of an info file,
                    # so report it.
                    # The trailing empty string makes this match
                    # entries scraped from the dir file,
                    # as described below.
                    print basename, $0, ""

                    # Also record it,
                    # so we can look up filenames later.
                    manual_aliases[basename] = $0
                }
            }

            # Inside the info dir file,
            # the entries we are interested in
            # look like one of these
            # (where ^? means ASCII DEL, 0x7F):
            #
            # * Some Alias: (manual-name).
            # * ^?Alias: With A Bonus Colon^?: (manual-name).
            # * Node Alias: (manual-name)Node name.
            # * ^?Node Alias: With A Bonus Colon^?: (manual-name)Node name.
            # * Node Alias: (manual-name)^?Node name. With period^?.
            #
            # The output we want to produce looks like:
            #
            # alias^Iinfofile^Inodename
            #
            # ...where ^I means ASCII TAB, 0x09.
            # The example entries above should produce the following output
            # (assuming "manual-name" represents /path/to/manual-name.info):
            #
            # Some Alias^I/path/to/manual-name.info^I
            # Alias: With A Bonus Colon^I/path/to/manual-name.info^I
            # Node Alias^I/path/to/manual-name.info^INode name
            # Node Alias: With A Bonus Colon^I/path/to/manual-name.info^INode name
            # Node Alias^I/path/to/manual-name.info^INode name. With period

            state == "infodir" && /^\* .*: / {
                # Trim the leading bullet
                line = substr($0, 3)

                # If the alias is DEL-quoted....
                if (line ~ /^\177/) {
                    # Trim the leading DEL
                    line = substr(line, 2)

                    # Match the rest of the alias
                    match(line, /^[^\177]*\177: /)
                    alias = substr(line, RSTART, RLENGTH-3)
                } else {
                    # Match the rest of the alias
                    match(line, /^[^:]*: /)
                    alias = substr(line, RSTART, RLENGTH-2)
                }
                line = substr(line, RLENGTH+1)

                # If there is a manual, grab it.
                if (line ~ /^\(/) {
                    line = substr(line, 2)
                    match (line, /^[^)]*)/)
                    manual_alias = substr(line, RSTART, RLENGTH-1)
                    line = substr(line, RLENGTH+1)
                } else {
                    # Default to the current manual,
                    # which is... uh.. the top-level directory?
                    manual_alias = "dir"
                }
                filename = manual_aliases[manual_alias]

                # If the node is DEL-quoted....
                if (line ~ /^\177/) {
                    # Trim the leading DEL
                    line = substr(line, 2)

                    # Match the rest of the alias
                    match(line, /^[^\177]*\177/)
                } else {
                    # Match the rest of the alias
                    match(line, /^[^.]*\./)
                }
                nodename = substr(line, RSTART, RLENGTH-1)

                # Ignore aliases whose manual
                # does not correspond to a file.
                if (filename) {
                    print alias, filename, nodename
                } else {
                    print infodir "/dir: " \
                        "manual " manual_alias " does not exist, " \
                        "ignoring alias " alias \
                        >> "/dev/stderr"
                }
            }
            '
}

list_nodes_for_file() {
    read_info_file "$1" |
        awk \
            -v infofile="$1" \
            -v infodir="$(dirname "$1")" \
            '
            BEGIN {
                OFS = "\t"

                state = "other"
            }

            /^\037$/ { state = "new node"; getline }

            # If this is a regular node with DEL-quoting...
            state == "new node" && /^File: [^,]*, +Node: \177/ {
                # ...then grab the node name,
                # since there might not be an index at the end.
                split($0, parts, /\177/)
                print parts[2]

                # Ignore the rest of the node
                state = "other"
            }

            # If this is a regular node without quoting...
            state == "new node" && /^File: [^,]*, +Node: / {
                match($0, /^File: [^,]*, +Node: /)
                if (RSTART) {
                    tail = substr($0, RLENGTH+1)
                    match(tail, /^[^,]*/)
                    print substr(tail, RSTART, RLENGTH)
                }

                # Ignore the rest of the node
                state = "other"
            }

            state == "new node" && /^Tag Table:$/ {
                state = "tag table"; getline
            }

           state == "tag table" {
                if (/^Node: /) {
                    split(substr($0, 7), tag_record, /\177/)
                    print tag_record[1]
                }
            }
            '
}

format_subfile() {
    read_info_file "$1" |
        awk '
            BEGIN { RS="\37\n"; FS="\n" }
            /^File: / { print }
            '
}

help() {
    echo "Usage:"
    echo "$0 help"
    echo "    Show this help"
    echo "$0 lsaliases"
    echo "    List aliases for manuals and manual nodes"
    echo "$0 locate [ALIAS]"
    echo "    Determine the file and node for the given alias,"
    echo "    or otherwise for the top level directory"
    echo "$0 lsnodes FILE"
    echo "    List nodes in FILE, a compiled TeXinfo file"
    echo "$0 lssubfiles FILE"
    echo "    List subfiles of FILE if it is split, or just FILE otherwise"
    echo "$0 format FILE"
    echo "    Format the text nodes in FILE, a compiled TeXinfo file"
}

lsaliases() {
    if [ "$#" -ne 0 ]; then
        help
        return 1
    fi

    list_info_dirs |
        while read -r infodir; do
            if [ -d "$infodir" ]; then
                names_for_info_dir "$infodir"
            fi
        done
}

locate() {
    if [ "$#" -gt 1 ]; then
        help
        return 1
    fi

    if [ -f "$1" ]; then
        # We already have a specific info file.
        printf "%s\t%s\n" "$1" ""
        return 0
    fi

    manual=${1:-dir}

    lsaliases |
        while IFS="$(printf '\t')" read -r currmanual currfile currnode; do
            if [ "$manual" = "$currmanual" ]; then
                printf "%s\t%s\n" "$currfile" "$currnode"
                break
            fi
        done
}

lsnodes() {
    if [ "$#" -ne 1 ]; then
        help
        return 1
    fi

    list_nodes_for_file "$1"
}

lssubfiles() {
    if [ "$#" -ne 1 ]; then
        help
        return 1
    fi

    read_info_file "$1" |
        awk -v infofile="$1" -v infodir="$(dirname "$1")" '
            BEGIN { FS=": " }

            /^\37$/ {
                getline
                if (/^Indirect:$/) {
                    # This is a split file, so read the filenames!
                    getline

                    while ($1 !~ /\37$/) {
                        newfile = infodir "/" $1
                        if (infofile ~ /\.gz/)
                            newfile = newfile ".gz"

                        print newfile

                        getline
                    }

                } else {
                    # This is a non-split file,
                    # use it as-is.
                    print infofile
                }

                exit
            }
            '
}

format() {
    lssubfiles "$1" |
        while read -r subfile; do
            format_subfile "$subfile"
        done
}

case "$1" in
    ""|-h|--help)
        help
        ;;
    *)
        "$@"
        ;;
esac
