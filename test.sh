#!/bin/sh
cd "$(dirname "$0")"

kak -n -e "
    source texinfo.kak
    texinfo ./testdata.info
    echo -quoting shell -to-file test-result-raw.txt %opt{texinfo_link_data}
    quit
    "

sed -e "s/' '/'\n'/g" < test-result-raw.txt > test-result-actual.txt
rm test-result-raw.txt
diff -urw test-result-actual.txt test-result-expected.txt
